#!/usr/bin/env python
import logging
import os
import time
import string
import sys
import jinja2
import re

import requests


logger = logging.getLogger(__name__)

# Config ENV
POSTGRES_PORT = os.getenv("POSTGRES_PORT")
TUTUM_CONTAINER_API_URL = os.getenv("TUTUM_CONTAINER_API_URL", None)
POLLING_PERIOD = max(int(os.getenv("POLLING_PERIOD", 30)), 5)
DUMP_DIR = os.getenv("DUMP_DIR")

TUTUM_AUTH = os.getenv("TUTUM_AUTH")
DEBUG = os.getenv("DEBUG", False)

# Const var
BACKUP_TEMPLATE = '/postgres-backup.sh.j2'
BACKUP_SCRIPT = '/postgres-backup.sh'

endpoint_match = re.compile(r"(?P<proto>tcp|udp):\/\/(?P<addr>[^:]*):(?P<port>.*)")

def get_backend_routes_tutum(api_url, auth):
    # Return sth like: {'HELLO_WORLD_1': {'proto': 'tcp', 'addr': '172.17.0.103', 'port': '80'},
    # 'HELLO_WORLD_2': {'proto': 'tcp', 'addr': '172.17.0.95', 'port': '80'}}
    session = requests.Session()
    headers = {"Authorization": auth}
    r = session.get(api_url, headers=headers)
    r.raise_for_status()
    container_details = r.json()

    addr_port_dict = {}
    for link in container_details.get("linked_to_container", []):
        for port, endpoint in link.get("endpoints", {}).iteritems():
            if port in "%s/tcp" % POSTGRES_PORT:
                addr_port_dict[link["name"].upper().replace("-", "_")] = endpoint_match.match(endpoint).groupdict()

    env = {}
    for env_var in container_details.get('container_envvars', []):
        env[env_var['key']] = env_var['value']

    return addr_port_dict, container_details, env

if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout)
    logging.getLogger(__name__).setLevel(logging.DEBUG if DEBUG else logging.INFO)

    # Load template
    f = open(BACKUP_TEMPLATE, 'r')
    template = jinja2.Template(f.read())
    f.close()

    # Main loop
    old_text = ""
    while True:
        try:
            backends, container, env = get_backend_routes_tutum(TUTUM_CONTAINER_API_URL, TUTUM_AUTH)

            # Update backend routes
            script = template.render(
                backends=backends,
                container=container,
                env=env,
                dump_dir=DUMP_DIR
            )

            # If cfg changes, write to file
            if old_text != script:
                f = open(BACKUP_SCRIPT, 'w')
                f.write(script)
                f.close()

                logger.info("/postgres-backup.sh has been changed")
                old_text = script

        except Exception as e:
            logger.exception("Error: %s" % e)

        time.sleep(POLLING_PERIOD)

