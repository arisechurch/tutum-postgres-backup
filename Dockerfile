FROM phusion/baseimage
MAINTAINER Tim Smart <tim.smart@arisechurch.com>

# BACKEND_PORT is the port of the app server which is load balanced
ENV POSTGRES_PORT 5432
ENV DUMP_DIR /var/lib/postgresql/dumps

VOLUME /var/lib/postgresql/dumps

# Install pip, requests and Jinja2
RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main' > \
   /etc/apt/sources.list.d/pgdg.list
RUN curl -s https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
    apt-key add -

RUN apt-get update && \
    apt-get install -y --no-install-recommends python-pip postgresql-client-9.4 && \
    apt-get clean && \
    pip install requests Jinja2 && \
    rm -rf /var/lib/apt/lists/*

# Create dump directory
RUN mkdir -p /var/lib/postgresql/dumps

# Add services
RUN mkdir /etc/service/postgres-backup-gen
ADD postgres-backup-gen.py /etc/service/postgres-backup-gen/run

# Add template
ADD postgres-backup.sh.j2 /postgres-backup.sh.j2

# Add cron job
RUN echo "0 * * * * root /bin/bash /postgres-backup.sh\n#" >> /etc/crontab

CMD ["/sbin/my_init"]
